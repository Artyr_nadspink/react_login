import React, {Component} from 'react';
import './style.css';
import Logo from '../../img/logo.png'
import Error from '../../img/error.png'
import Yes from '../../img/yes.png'

class Login extends Component {
    state = {
        chek_email: null,
        chek_password: null,
        password_chek: false,
        email: '',
        password: '',
    };
    onSubmit = () => {
        if (this.state.email === "") {
            this.setState({chek_email: true})
        }else{
            this.setState({chek_email: false})
        }
        if (this.state.password === "") {
            this.setState({chek_password: true})
        }else{
            this.setState({chek_password: false})
        }

    }
    onChange = (e) => {
        this.setState({[e.target.name]: e.target.value});

    }

    render() {
        const {email, password} = this.state;
        return (
            <div className="root">
                <img src={Logo} alt=""/>
                <div className="form">
                    <div className="content">
                        <div className="container email">
                            <input
                                onChange={this.onChange}
                                value={email} name="email"
                                type="text"
                                className={this.state.chek_email ? "red email_inp" : "email_inp"}
                                placeholder="E-mail"/>
                            <p className={this.state.chek_email ? "error" : ""}>Invalid Username</p>
                            {this.state.chek_email ?  <img className="img_error"  src={Error} alt=""/> : ""
                               }
                            {this.state.chek_email===false ?  <img className="img_error"  src={Yes} alt=""/>  : ""}

                        </div>
                        <div className="container password">
                            <input type="password"
                                   className="password_inp"
                                   className={ this.state.chek_password  ? "red password_inp" :
                                              this.state.chek_password===false ? "password_inp password_ok": " password_inp" }
                                   onChange={this.onChange}
                                   value={password}
                                   name="password"
                                   placeholder="Password"/>
                            {console.log(this.state.chek_password)}
                            {this.state.chek_password===false ?  <img  src={Yes} alt=""/>  : ""}
                        </div>
                        <button onClick={this.onSubmit}>Login</button>
                        <span>Forgot your password? <a href="#">Reset it here</a></span>
                    </div>
                </div>
            </div>
        );
    }
}

export default Login;
